# Before uploading files via the Framagit web interface, read this please

Pushing directly to the “master” branch has now been disabled, to allow changes to be reviewed
before they are final, and to be able to better keep track of what has changed.

If you're used to uploading your files on Framagit, in practice this means you'll have to do one
little extra step: creating a branch to work on. You can create a new branch by pressing the `+`
icon and selecting “New branch”.  A good name for your branch is `epXX-LL` where XX is the number
of the episode and LL is the code for your language. Keep “create from” at “master”.

When you have created the branch, you'll be automatically on it: instead of “master” you'll see the
name of your branch. From there you can proceed as usual.

If you have a problem or question, don't be afraid to [reach out to the rest of the community](https://peppercarrot.com/en/static4/community) on one of the chat channels.

Thanks for translating!
