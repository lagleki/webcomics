# Contributing translations or corrections

![cover image](https://www.peppercarrot.com/data/images/lab/2015-03-01_translation.jpg)

**Translating or correcting an episode is fast, fun and easy!**  
You can do it from any type of computer (Windows, Mac, Linux and more), using only free/libre tools.

## What you need

- **[Inkscape](https://inkscape.org/en/)**: the free/libre and open-source vector drawing software.
- **[The source repository](https://framagit.org/peppercarrot/webcomics "Git repository")**: it contains all translations of Pepper&Carrot (around 400MB). You can clone it if you are familiar with Git or [directly download its files](https://framagit.org/peppercarrot/webcomics/repository/master/archive.zip). If you prefer to get the sources of a single episode; just select it in [the sources](https://www.peppercarrot.com/en/static6/sources&page=episodes) and download its _Translator's pack_ (big grey button on top).
- **[The fonts](fonts)**: They are already part of the source repository in the subfolder fonts/. For installation instructions, please refer to the [README.md](fonts/README.md) file in that directory.


## Connect with us!

[The online bug tracker](https://framagit.org/peppercarrot/webcomics/issues) on Framagit is the place for all discussions related to translation. It's our official forum. With a bit of organization, we can avoid duplicate effort. So it might be a good idea to check if someone is already working on the translation you want to add. If no one is working on the episode you want to translate, then it's probably a good idea to [open a new issue](https://framagit.org/peppercarrot/webcomics/issues/new?issue) to let others know you'll be working on a new translation _(Example title: "WIP: ep20 French translation")_.


## Create your folder

In the source, open an episode folder of your choice and then go to the sub-folder lang/. Duplicate an existing language and rename it to the language you want to translate. On Pepper&Carrot we use two letters only, based on [ISO language codes](http://www.w3schools.com/tags/ref_language_codes.asp). But if your target language doesn't have any two letter ISO code, we can invent one.

![](https://www.peppercarrot.com/data/images/lab/2015-03-02_tuto-translation/2015-03-01_d_duplicate-your-folder.jpg)  
_screenshot of duplicating a folder (copy/paste), then renaming it to a fictive language named 'my'_


## Start translating

### Working on your own

Inside the folder you just created, just edit all the SVG files with Inkscape. Translate the content of speechbubbles, change the sound effects, resize speechbubbles to make them look good with your new content. Feel free to be the art director of your translation, and resize, arrange the layout to your taste. Don't forget to sign your translation on the bottom of the credit page and add your name in the [AUTHORS.md](AUTHORS.md) file (at the root of the  **webcomics** repository) as well.

![](https://www.peppercarrot.com/data/images/lab/2015-03-02_tuto-translation/2015-03-01_e_edit-your-inkscape-svg.jpg)

### Working with others

If you'd like to work with other people on the [Framagit peppercarrot/webcomics](https://framagit.org/peppercarrot/webcomics/) repo, it's easy to register on Framagit.

You can check if a group for your language already exists on [our Framagit group](https://framagit.org/peppercarrot/translators/).

* You can join it if it exists, by asking to be made a member of it.
* You can ask for a new group to be set up by adding your comment to this issue: [Create group for translators](https://framagit.org/peppercarrot/webcomics/issues/7).


## Send us your work

There are three ways of sending us your work.

- **Using Git**: You can make [merge requests](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html "GitLab documentation about merge requests") on our repository. If you become a regular contributor, we'll make you a [member](https://framagit.org/groups/peppercarrot/-/group_members) of the Framagit group with the permission to edit and push the files directly in the repo.
- **Using the bug tracker**: Pack your language folder into a ZIP file; it's very lightweight (no need of the whole repository) and attach your translation to a [new issue](https://framagit.org/peppercarrot/webcomics/issues/new?issue) on our bug tracker. Don't forget to tell how we should credit you in the AUTHORS.md file.
- **By email**: Pack your language folder into a ZIP file; it's very lightweight (no need of the whole repository) and attach it to an email to [info@davidrevoy.com](mailto:info@davidrevoy.com). Don't forget to tell how we should credit you in the AUTHORS.md file.

**Bravo! It's done :-)** All SVG files added to the the source repository will be rendered and posted online on [www.peppercarrot.com](https://www.peppercarrot.com/)!


## Translating the website

You can also propose a translation for the interface of the website. It isn't very long; around 300 lines of keywords and small paragraphs. The website has its [own repository](https://framagit.org/peppercarrot/website "Git repository") and language files are located in /themes/peppercarrot-theme_v2/lang.  

![](https://www.peppercarrot.com/data/images/lab/2015-03-02_tuto-translation/2015-03-01_g_about-website.jpg)  


## FAQ

**Q: Can I add a custom font?**  
A: Yes, you can add a new font for your language, but it needs to be published under a free licence or released in the public domain. The following fonts are accepted: Public domain fonts, GNU/GPL fonts, CC-0 fonts, CC-BY fonts, SIL Open Font License (OFL). Please also add the font information (author, license, link) as is done for other fonts in the repository. A trusted source for finding new open fonts: [http://openfontlibrary.org/](http://openfontlibrary.org/ "http://openfontlibrary.org/").

**Q: Can I translate the main title "Pepper&Carrot"?**  
A: Yes. Tharinda Divakara created a tutorial about how to create a cool vector title with Pepper&Carrot style. You can [read it here](https://www.peppercarrot.com/data/images/lab/2015-02-21_Multilingue-SVG-researches/2015-03-04_Sinhala-title-translation_how-to_by-Tharinda-Divakara.jpg "Tutorial about creating vector titles in Pepper&Carrot style").

**Q: Can I change the characters' names?**  
A: Sure, you can. For the French translation, I kept the english 'Carrot' because the French name "Carotte" is feminine. I kept 'Pepper' and didn't use the translation 'Poivre' or 'Poivron' because it sounds like an insult French-speakers may throw at a drunkard, 'Poivrot!'. To keep track of all international names, we have [a spreadsheet](translation-names-references.fods) at the root of the repository. It's a \*.fods file you can open with [LibreOffice Calc](https://www.libreoffice.org/).

**Q: A sentence doesn't sound good in my language, can I rewrite it?**  
A: That's ok. The most important is to keep the semantic, the feeling and the information of the story. Adding style and changing the expression of your target language is natural.

**Q: Will I need to translate all the episodes?**  
A: Feel free to contribute within your possibilities: you can translate one episode at a time. The website is designed to fall back to English if there's no translation available for a given episode. So, you can send episode 01, then one month later send episode 03, 04, 05 in a row, then stop here if you want... or then send all the other episodes. Every contribution is welcome; thank you for your effort and contributions!

**Q:** **Where can I get an overview of all available translations?**  
A: We maintain [a big table](https://www.peppercarrot.com/en/static6/sources&page=translation) on the official website to get an overview of the translation efforts.

**Q:** **How do you make a word in a speechbubble bold in Inkscape?**  
A: Select only the text you want to make bold, then change the font style to the bold one. This [picture](https://www.peppercarrot.com/data/images/faq/2018-07-01_bold-Lavi-fix.png) shows how to do that.  
If the font you are using doesn't have a bold variant (for example the old Lavi), you can select the text and add a StrokePaint to it, and change the StrokeStyle width. Here is a [picture](https://www.peppercarrot.com/data/images/faq/2018-02-22_bold-text-faq.jpg) showing how I'm doing it.

**Q:** **What about the license of translations?**  
A: Perfect transition, read the next chapter.


## License: terms and conditions

By submitting any content to the Pepper&Carrot [webcomics repository](https://framagit.org/peppercarrot/webcomics), such as new translations, improvements to translations, issues, or other content, (“Your Content”), you accept that you release it under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/) License (“CC BY 4.0”). You acknowledge that you understand this license and you permit everyone to use Your Content under this license. You warrant that you have the right to grant this permission and you understand that under the terms of the CC BY 4.0 license you cannot later revoke it.

You are of course still free to make derivatives and translations of Pepper&Carrot and distribute them under other licenses, so long as you comply with the applicable licenses. But if you wish to have them included in this repository or on [the official website](https://www.peppercarrot.com/), you must release them under CC BY 4.0.

(This does not apply to files in the `fonts/` directory, which are released under their own separate license agreements.)

All the attributions are listed on the [AUTHOR.md](https://framagit.org/peppercarrot/webcomics/blob/master/AUTHORS.md) files at the root of the repository and also under the [Author](https://www.peppercarrot.com/en/static7/author) menu on [the main website](https://www.peppercarrot.com/ "peppercarrot.com").
